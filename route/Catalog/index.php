<!DOCTYPE html>
<head>
    <title>Каталог продукции</title>
    <style type="text/css">
        body {
            font: 10pt Arial, Helvetica, sans-serif; /* Шрифт на веб-странице */
            background: #A9A9A9; /* Цвет фона */
        }
        h2 {
            font-size: 1.1em; /* Размер шрифта */
            color: #1E90FF; /* Цвет текста */
            margin-top: 0; /* Отступ сверху */
        }
        #container {
            width: 500px; /* Ширина слоя */
            margin: 0 auto; /* Выравнивание по центру */
            background: #f0f0f0; /* Цвет фона левой колонки */
        }
        #header {
            font-size: 2.2em; /* Размер текста */
            text-align: center; /* Выравнивание по центру */
            padding: 5px; /* Отступы вокруг текста */
            background: #1E90FF; /* Цвет фона шапки */
            color: #ffe; /* Цвет текста */
        }
        #sidebar {
            margin-top: 10px;
            width: 110px; /* Ширина слоя */
            padding: 0 10px; /* Отступы вокруг текста */
            float: left; /* Обтекание по правому краю */
        }
        #content {
            margin-left: 130px; /* Отступ слева */
            padding: 10px; /* Поля вокруг текста */
            background: #fff; /* Цвет фона правой колонки */
        }
        #footer {
            background: #1E90FF; /* Цвет фона подвала */
            color: #fff; /* Цвет текста */
            padding: 5px; /* Отступы вокруг текста */
            clear: left; /* Отменяем действие float */
        }
    </style>
</head>
<body>
    <div id="header">Каталог продукции</div>
    <div id="sidebar">
        <p><a href="">Борсодержащая сталь</a></p>
        <p><a href="">Высокопрочная сталь</a></p>
        <p><a href="">Калиброванная сталь</a></p>
    </div>
    <div id="content">
        <h2>Металлопрокат и трубы</h2>
        <ul>
            <li>Сортовой прокат;</li>
            <li>Холоднокатаный прокат;</li>
            <li>Горячекатаный прокат;</li>
            <li>Трубы и гнутые профили;</li>
            <li>Трубы большого диаметра;</li>
        </ul>
    </div>
    <div id="footer">&copy; 2018 Project</div>
</div>
</body>
</html>