<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Новости</title>
    <style type="text/css">
        body {
            font: 10pt Arial, Helvetica, sans-serif; /* Шрифт на веб-странице */
            background: #D3D3D3; /* Цвет фона */
            margin: 0; /* Убираем отступы */
        }
        h2 {
            font-size: 1.1em; /* Размер шрифта */
            color: #752641; /* Цвет текста */
            margin-bottom: 0; /* Отступ снизу */
        }
        #container {
            width: 500px; /* Ширина макета */
            margin: 0 auto; /* Выравниваем по центру */
            background: #f0f0f0; /* Цвет фона правой колонки */
        }
        #header {
            background: #8fa09b; /* Цвет фона */
            font-size: 24pt; /* Размер текста */
            font-weight: bold; /* Жирное начертание */
            color: #edeed5; /* Цвет текста */
            padding: 5px; /* Отступы вокруг текста */
        }
        #content {
            float: left; /* Обтекание по правому краю */
            width: 329px; /* Ширина левой колонки */
            padding: 10px; /* Поля вокруг текста */
            border-right: 1px dashed #183533; /* Линия справа */
            background: #fff; /* Цвет фона левой колонки */
        }
        #content p {
            margin-top: 0.3em /* Отступ сверху */
        }
        #sidebar {
            float: left; /* Обтекание по правому краю */
            width: 120px; /* Ширина */
            padding: 10px; /* Отступы вокруг текста */
        }
        #footer {
            background: #8fa09b; /* Цвет фона */
            color: #fff; /* Цвет текста */
            padding: 5px; /* Отступы вокруг текста */
        }
        .clear {
            clear: both; /* Отмена обтекания */
        }
    </style>
</head>
<body>
    <div id="header">Пресс-центр</div>
    <div id="content">
        <h2>29/07/2021</h2>
        <p>Успешно завершены испытания нового поколения труб большого диаметра для строительства магистральных
            газопроводов</p>
        <h2>22/07/2021</h2>
        <p>Впервые поставлены трубы большого диаметра в Бразилию</p>
        <h2>12/07/2021</h2>
        <p>Опубликован отчет об устойчивом развитии за 2020 год</p>
    </div>
    <div id="sidebar">
        <p><a href="">Финансы</a></p>
        <p><a href="">Люди</a></p>
        <p><a href="">Безопасность</a></p>
        <p><a href="">Производство</a></p>
        <p><a href="">Сбыт продукции</a></p>
    </div>
    <div class="clear"></div>
    <div id="footer">&copy;2018 Project</div>
</div>
</body>
</html>
