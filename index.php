<?php
include 'functions.php';
$auth = checkAuth();
?>

<?php include 'header.php';?>

<div class="left-collum-index">
    <h1>Возможности проекта —</h1>
        <p>Вести свои личные списки, например покупки в магазине, цели, задачи и многое другое.
             Делится списками с друзьями и просматривать списки друзей.</p>
</div>

<div class="right-collum-index">
    <div class="project-folders-menu">
        <ul class="project-folders-v">
            <li class="project-folders-v-active"><a href="?authorization=yes">Авторизация</a></li>
            <li><a href="#">Регистрация</a></li>
            <li><a href="#">Забыли пароль?</a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
    <div class="index-auth">
        <form style="<?= showAuth() ?>" action="" method="post"></form>
    <div class="iat"></div>
        <label for="login_id">Ваш e-mail:</label>
        <input id="login_id" size="30" name="login"
               value= "<?= checkForm('login', $auth) ?>">

        <div class="iat"></div>
        <label for="password_id">Ваш пароль:</label>
        <input id="password_id" size="30" name="password" type="password"
               value= "<?=checkForm('password', $auth)?>">
        <div class input type="submit" value="Войти" </div>

                    <?php
                    if (checkPost("login") && checkPost("password")) { //создали if в которой проверили
                        // существование POST с ключами login и password. Если выполнилось условие, то проверяем авторизацию
                        // пользователя.
                        if ($auth) {
                            require_once 'include/success.php';
                        } else {
                            require_once 'include/error.php';
                        }
                    }
                    ?>
                </div>
            </td>
        </tr>
    </table>
<?php include 'footer.php';?>
