<?php
function checkPost($key)
{
    return isset($_POST[$key]); //функция возвращает тру если есть переменная пост с ключом ки
}

function checkForm($passLog, $auth)
{
    if (!$auth) {
        $logPass = $_POST[$passLog];
    } else {
        $logPass = '';
    }
    return $logPass;
}

function checkAuth() // поместил в функцию if для правильности кода.
{
    require 'login.php'; //подключаем файлы с логинами и паролями
    require 'password.php';
    $numberOfUser = false; //если нет то false
    $authorization = false;
    if (checkPost("login")) { //если существует то
        foreach ($logins as $numberOfLogin => $login) { //прохожу по всем элементам массива $logins
            if ($login === $_POST["login"]) { // если $login будет равен значению параметра POST
                $numberOfUser = $numberOfLogin; //сохраняем номер этого логина
            }
        }
        if (checkPost("password")) {
            if ($passwords[$numberOfUser] === $_POST["password"]) { //если пароль нашего пользователя равен паролю введен-у от поль-я то авторизация пройдена
                $authorization = true;
                $_GET['authorization'] = 'no'; // скрывает данные при верной авторизации
            }
        }
    }
    return $authorization;
}

function showAuth()
{
    if ($_GET['authorization'] == 'yes') {
        $display = 'display:block';
    } else {
        $display = 'display:none';
    }
    return $display;
}
