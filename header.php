<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="css/styles.css" rel="stylesheet">
    <title>Project - ведение списков</title>
</head>

<body>
<div class="header">
    <div class="logo"><img src="i/logo.png" width="68" height="23" alt="Project"></div>
    <div class="clearfix"></div>
</div>

<div class="clearfix">
    <ul class="main-menu top">
        <li><a href="route/Main/index.php">Главная</a></li>
        <li><a href="route/AboutUs/index.php">О нас</a></li>
        <li><a href="route/Contacts/index.php">Контакты</a></li>
        <li><a href="route/News/index.php">Новости</a></li>
        <li><a href="route/Catalog/index.php">Каталог</a></li>
    </ul>
</div>
